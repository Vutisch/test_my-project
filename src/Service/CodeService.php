<?php

namespace App\Service;

use App\Entity\Code;
use App\Repository\CodeRepository;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
Use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class CodeService
 * @package App\Service
 */
class CodeService
{
    private $codeRepository;


    public function __construct(CodeRepository $codeRepository )
    {
        $this->codeRepository = $codeRepository;

    }
    /**
     * @param int $codeValue
     * @return
     *
     */
    public function getCode(string $codeValue)
    {
        $code = $this->codeRepository->findByCode($codeValue);
        if (empty($code)) {
            return false;
        }

        return $code[0];
    }

    /**
     * @return array|null
     */
    public function getAllCode(): ?array
    {
        return $this->codeRepository->findAll();
    }

    /**
     * @param int $nb
     * @param null $export
     * @return Response
     */
    public function addCode($nb = 1,$export = null): Response
    {
        $i = 0;
        while ($i<$nb){
            do {
                $cod =$this->generateCode();
            } while ($this->getCode($cod)) ;

            $code = new Code();
            $code->setCode($cod);
            $code->setData(new \DateTime("now", new \DateTimeZone("UTC")));
            $this->codeRepository->save($code);
            $i++;
            $listCodes[$i]=$code->getCode();
        }

        $respouns =$this->showResponse( $listCodes);
        $export ==='xls'? $respouns = $this->export($listCodes): true;

        return $respouns;
    }

    /**
     * @param int $codeValue
     * @param string $cod
     * @return Code
     * @throws EntityNotFoundException
     */
    public function updateCode(int $codeValue, string $cod): Code
    {
        $code = $this->codeRepository->findByCode($codeValue);
        if (!$code) {
            throw new EntityNotFoundException('Code: '.$code.' does not exist!');

        }
        $code->setCode($cod);
        $code->setData(gmdate("Y-m-d\TH:i:s\Z"));
        $this->codeRepository->save($code);
        return $code;
    }

    /**
     * @param string $CodeValue
     * @throws EntityNotFoundException
     */
    public function deleteCode(string $codeValue): void
    {
        $code = $this->codeRepository->findByCode($codeValue);
        if (!$code) {
            throw new EntityNotFoundException('Code: '.$code.' does not exist!');
        }
        $this->codeRepository->delete($code);
    }

    /**
     * @return string
     */
    public function generateCode():string
    {
        $countNumber = 0;
        $countChar = 0;

        $characters = array(
            "number" =>'23456789',
            "char" =>'ABCDEF');
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            if ($countNumber === 4) {
                $value ="char";
            } elseif ($countChar === 6) {
                $value ="number";
            } else {
                $value = $this->random();
            }

            $charactersLength = strlen($characters[$value]);
            $randomString .= $characters[$value][rand(0, $charactersLength - 1)];
            $value ==='number'? $countNumber++ : $countChar++;
        }

        return $randomString;
    }

    /**
     * @return string
     */
    public function random(): string
    {
        $col = rand (0,1);
        if ($col){
            return 'number';
        }

        return 'char';
    }

    /**
     * @param array $listCodes
     * @return BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function export(array $listCodes):Response
    {
        $filename = 'code.xlsx';

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($listCodes as $key => $value){
            $sheet->setCellValue('A'.$key, $value);
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($filename);

        $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    /**
     * @param array $array
     * @return Response
     */
    public function showResponse(array $array):Response
    {
        $response = new Response();
        $response->setContent(json_encode(array( $array)));
        $response->headers->set('Content-Type', 'application/json','charset=UTF-8');

        return $response;
    }
}