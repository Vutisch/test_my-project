<?php
/**
 * Created by PhpStorm.
 * User: vutisch
 * Date: 19.10.18
 * Time: 20:42
 */

namespace App\Entity;

use Doctrine\DBAL\Types\DateTimeTzType;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * Class Article
 * @ORM\Entity
 * @package App\Entiry\Code
 */
class Code
{
    /**
     **
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="code", type="string",length=10,unique=true)
     */
    private $code;

    /**
     * @ORM\Column( type="datetime", nullable=true)
     */
    private $data;

    public function getId()
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function getData(): \DateTime
    {
        return $this->data;
    }

    public function setData( $data): self
    {
        $this->data = $data;
        return $this;
    }
}