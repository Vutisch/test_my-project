<?php
/**
 * Created by PhpStorm.
 * User: vutisch
 * Date: 19.10.18
 * Time: 20:46
 */
namespace App\Repository;

use App\Entity\Code;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;


class CodeRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * CodeRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager= $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Code::class);
        $this->objectRepository->findAll();
    }

    /**
     * @param string $codeValue
     * @return array
     */
    public function findByCode(string $codeValue): Array
    {
        return $this->objectRepository->findBy(['code' =>$codeValue]);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }
    /**
     * @param Code $code
     */
    public function save(Code $code): void
    {
        $this->entityManager->persist($code);
        $this->entityManager->flush();
    }
    /**
     * @param Code $code
     */
    public function delete(Code $code): void
    {
        $this->entityManager->remove($code);
        $this->entityManager->flush();
    }
}