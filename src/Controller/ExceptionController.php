<?php
/**
 * Created by PhpStorm.
 * User: vutisch
 * Date: 22.10.18
 * Time: 0:29
 */
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;


class ExceptionController extends Controller {

    CONST CLASS_ERROR ='Symfony\Component\HttpKernel\Exception\NotFoundHttpException';

    /**
     * @param FlattenException $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FlattenException $exception) {

        if ($exception->getClass() === self::CLASS_ERROR & empty($_SERVER['PATH_INFO'])) {
            return $this->render('bundles/TwigBundle/Exception/error404.html.twig');
        }

        return $this->render('bundles/TwigBundle/Exception/error500.html.twig');
    }
}