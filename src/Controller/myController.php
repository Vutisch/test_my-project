<?php

namespace App\Controller;

use App\Service\CodeService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class myController extends FOSRestController
{
    /**
     * @var CodeService
     */
    private $codeService;

    /**
     * myController constructor.
     * @param CodeService $codeService
     */
    public function __construct(CodeService $codeService)
    {
        $this->codeService = $codeService;

    }

    /**
     * @Rest\Post("/generate")
     * @param Request $request
     */

    public function postIndex(Request $request)
    {
       $nb = $request->request->get('nb');
       $nb === null ? $nb = 1: true ;
       $export = $request->request->get('export');

       return $this->codeService->addCode($nb, $export);
    }

    /**
     * @Rest\Get("/")
     * @param Request $request
     */
    public function getIndex(Request $request): Response
    {
        $code = $request->query->get('code');
        $myCode = $this->codeService->getCode($code);

        if(!$myCode) {
            throw new NotFoundHttpException();
        }

        $array = array(
            'id' => $myCode->getId(),
            'code' => $myCode->getCode(),
            'data' => $myCode->getData(),
        );

        return $this->codeService->showResponse($array);
    }

}